package org.randombits.supplier.confluence.user;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.mail.address.ConfluenceMailAddress;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.PersonalInformationManager;
import com.atlassian.confluence.user.UserDetailsManager;
import com.atlassian.user.User;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.mail.internet.InternetAddress;

import java.io.UnsupportedEncodingException;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.when;

/**
 * @author williamtanweichun
 * @since 1.0.12.20151012
 */
@RunWith(MockitoJUnitRunner.class)
public class UserSupplierTest {

    UserSupplier $;
    @Mock User user;
    @Mock UserDetailsManager userDetailsManager;
    @Mock ContentPropertyManager contentPropertyManager;
    @Mock PersonalInformationManager personalInformationManager;
    @Mock SettingsManager settingsManager;
    @Mock Settings settings;

    String phone = randomAlphanumeric(10);
    String im = randomAlphanumeric(10);
    String website = randomAlphanumeric(10);
    String position = randomAlphanumeric(10);
    String department = randomAlphanumeric(10);
    String location = randomAlphanumeric(10);
    String fullname = randomAlphanumeric(10);
    String emailAddress = randomAlphanumeric(6) + "@" + randomAlphanumeric(5) + "." + randomAlphanumeric(3);

    @Before public void setup() {
        $ = new UserSupplier() {
            protected boolean isGlobalAdministrator(User user) {
                return true;
            }
        };

        $.setSettingsManager(settingsManager);
        $.setUserDetailsManager(userDetailsManager);
    }

    @Test public void shouldReturnPhone() {
        when(userDetailsManager.getStringProperty(user, "phone")).thenReturn(phone);
        assertThat($.getPhone(user), is(phone));
    }

    @Test public void shouldReturnIM() {
        when(userDetailsManager.getStringProperty(user, "im")).thenReturn(im);
        assertThat($.getIM(user), is(im));
    }

    @Test public void shouldReturnWebsite() {
        when(userDetailsManager.getStringProperty(user, "website")).thenReturn(website);
        assertThat($.getWebsite(user), is(website));
    }

    @Test public void shouldReturnPosition() {
        when(userDetailsManager.getStringProperty(user, "position")).thenReturn(position);
        assertThat($.getPosition(user), is(position));
    }

    @Test public void shouldReturnDepartment() {
        when(userDetailsManager.getStringProperty(user, "department")).thenReturn(department);
        assertThat($.getDepartment(user), is(department));
    }

    @Test public void shouldReturnLocation() {
        when(userDetailsManager.getStringProperty(user, "location")).thenReturn(location);
        assertThat($.getLocation(user), is(location));
    }

    @Test public void shouldReturnConfluenceMailAddressObject() throws UnsupportedEncodingException {
        when(user.getEmail()).thenReturn(emailAddress);
        when(user.getFullName()).thenReturn(fullname);
        when(settingsManager.getGlobalSettings()).thenReturn(settings);
        ConfluenceMailAddress confluenceMailAddress = new ConfluenceMailAddress(new InternetAddress(emailAddress, fullname));

        assertThat($.getUserEmail(user), Is.<Object>is(confluenceMailAddress));
    }
}