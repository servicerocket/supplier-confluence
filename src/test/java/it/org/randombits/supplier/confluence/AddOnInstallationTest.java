package it.org.randombits.supplier.confluence;

import com.atlassian.confluence.it.User;
import com.atlassian.confluence.webdriver.AbstractWebDriverTest;
import it.com.servicerocket.randombits.AddOnTest;
import it.com.servicerocket.randombits.pageobject.AddOnOSGIPage;
import org.junit.Test;

/**
 * @author Kai Fung
 * @since 1.0.9.20141014
 */
public class AddOnInstallationTest extends AbstractWebDriverTest {

    @Test public void testSupplierConfluencePluginInstalled() throws Exception {
        assert new AddOnTest().isInstalled(
                product.login(User.ADMIN, AddOnOSGIPage.class),
                "RB Supplier - Confluence"
        );
    }
}
