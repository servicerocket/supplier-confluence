package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.mail.Mail;
import com.atlassian.confluence.mail.address.ConfluenceMailAddress;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.core.annotate.SupportedTypes;
import org.randombits.utils.lang.API;

import java.util.Date;

@SupplierPrefix("mail")
@SupportedTypes(Mail.class)
public class MailSupplier extends AbstractSpaceContentSupplier<Mail> {

    @SupplierKey("sent date")
    @API("1.0.0")
    public Date getSentDate(@KeyValue Mail mail) {
        return mail.getSentDate();
    }

    @SupplierKey("recipient addresses")
    @API("1.0.0")
    public ConfluenceMailAddress[] getRecipientAddresses(@KeyValue Mail mail) {
        return mail.getRecipients();
    }

    @SupplierKey("message id")
    @API("1.0.0")
    public String getMessageId(@KeyValue Mail mail) {
        return mail.getMessageId();
    }

    @SupplierKey("message body")
    @API("1.0.0")
    public String getMessageBody(@KeyValue Mail mail) {
        return mail.getMessageBody();
    }

    @SupplierKey("in reply to")
    @API("1.0.0")
    public String getInReplyTo(@KeyValue Mail mail) {
        return mail.getInReplyTo();
    }

    @SupplierKey("from addresses")
    @API("1.0.0")
    public ConfluenceMailAddress[] getFromAddresses(@KeyValue Mail mail) {
        return mail.getFrom();
    }

    @SupplierKey("subject")
    @API("1.0.0")
    public String getSubject(@KeyValue Mail mail) {
        return mail.getDisplayableSubject();
    }

    @Override
    protected String getResultType() {
        return Mail.CONTENT_TYPE;
    }
}
