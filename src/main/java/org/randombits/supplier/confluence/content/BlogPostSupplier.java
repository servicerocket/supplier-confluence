package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.pages.BlogPost;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.core.annotate.SupportedTypes;
import org.randombits.utils.lang.API;

import java.util.Date;

@SupplierPrefix({"news", "blogpost"})
@SupportedTypes(BlogPost.class)
public class BlogPostSupplier extends AbstractPageSupplier<BlogPost> {

    /**
     * Constructs an annotated supplier.
     */
    public BlogPostSupplier() {
    }

    @SupplierKey("date path")
    @API("1.0.0")
    private String getDatePath(@KeyValue BlogPost content) {
        return content.getDatePath();
    }

    @SupplierKey("posting date")
    @API("1.0.0")
    private Date getPostingDate(@KeyValue BlogPost content) {
        return content.getCreationDate();
    }

    @Override
    protected String getResultType() {
        return BlogPost.CONTENT_TYPE;
    }
}
