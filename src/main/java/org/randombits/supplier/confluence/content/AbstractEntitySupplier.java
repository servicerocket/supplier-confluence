/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.Versioned;
import com.atlassian.confluence.links.LinkManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.renderer.links.LinkResolver;
import com.atlassian.user.User;
import org.randombits.storage.ResourceStorage;
import org.randombits.supplier.confluence.AbstractConfluenceSupplier;
import org.randombits.supplier.core.DisplayableSupplier;
import org.randombits.supplier.core.LinkableSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.utils.lang.API;
import org.randombits.utils.lang.ClassUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * Supplies information about Confluence entities. All subclasses of this class
 * will support {@link SearchResult}, and will automatically convert from a SearchResult
 * to a Page/Annotation/etc as required by the particular method. If subclasses wish to
 * provide a specific method to handle a key as a SearchResult, simply provide a method
 * with a higher weight than the equivalent for the ContentEntityObject instance. For example,
 *
 * @author David Peterson
 */
@SupplierPrefix(AbstractEntitySupplier.CONTENT_PREFIX)
@SupportedTypes(SearchResult.class)
public abstract class AbstractEntitySupplier<T extends ConfluenceEntityObject> extends AbstractConfluenceSupplier implements LinkableSupplier, DisplayableSupplier {

    public static final String CONTENT_PREFIX = "content";

    protected static final String ID_KEY = "id";

    protected static final String TYPE_KEY = "type";

    protected static final String TITLE_KEY = "title";

    protected static final String URL_KEY = "url";

    protected static final String ICON_KEY = "icon";

    protected static final String CREATOR_KEY = "creator";

    protected static final String CREATION_DATE_KEY = "creation date";

    protected static final String MODIFIER_KEY = "modifier";

    protected static final String MODIFICATION_DATE_KEY = "modification date";

    protected static final String VERSION_KEY = "version";

    protected static final String IS_LATEST_VERSION_KEY = "is latest version";

    protected static final String LATEST_VERSION_KEY = "latest version";

    protected static final String EQUALS_KEY = "equals {other entity}";

    protected static final String SELF = "@self";

    protected static final String PARENT = "@parent";

    private LinkManager linkManager;

    private LinkResolver linkResolver;

    private LinkAssistant linkAssistant;

    private ResourceStorage resources;

    private final Class<T> entityType;


    /**
     * Constructs an annotated supplier.
     */
    @SuppressWarnings("unchecked")
    public AbstractEntitySupplier() {
        entityType = (Class<T>) ClassUtils.getTypeArguments( AbstractEntitySupplier.class, getClass() ).get( 0 );
    }

    /**
     * Adds a check that the specific type of a SearchResult matches the target result type.
     *
     * @param value The value;
     * @return <code>true</code> if the value is supported.
     */
    @Override
    protected boolean checkSupportedValue( Object value ) {
        if ( value instanceof SearchResult ) {
            return getResultType().equals( ( (SearchResult) value ).getType() );
        }
        return super.checkSupportedValue( value );
    }

    protected abstract String getResultType();

    @SupplierKey(CREATOR_KEY)
    @KeyWeight(5)
    @API("1.0.0")
    public Object getCreator( @KeyValue SearchResult result ) {
        return getUser( result.getCreator() );
    }

    @SupplierKey(CREATOR_KEY)
    @API("1.0.0")
    public Object getCreator( @KeyValue T entity ) {
        return getUser( entity.getCreatorName() );
    }

    @SupplierKey(MODIFIER_KEY)
    @KeyWeight(5)
    @API("1.0.0")
    public Object getModifier( @KeyValue SearchResult result ) {
        return getUser( result.getLastModifier() );
    }

    @SupplierKey(MODIFIER_KEY)
    @API("1.0.0")
    public Object getModifier( @KeyValue T entity ) {
        return getUser( entity.getLastModifierName() );
    }

    @SupplierKey(VERSION_KEY)
    @KeyWeight(5)
    @API("1.0.0")
    public Integer getVersion( @KeyValue SearchResult result ) {
        return result.getContentVersion();
    }

    @SupplierKey(VERSION_KEY)
    @API("1.0.0")
    public Integer getVersion( @KeyValue T entity ) {
        if ( entity instanceof Versioned )
            return ( (Versioned) entity ).getVersion();
        return null;
    }

    @SupplierKey(IS_LATEST_VERSION_KEY)
    @API("1.0.0")
    public Boolean isLatestVersion( @KeyValue T entity ) {
        if ( entity instanceof Versioned )
            return ( (Versioned) entity ).isLatestVersion();
        return null;
    }

    @SupplierKey(LATEST_VERSION_KEY)
    @API("1.0.0")
    public Versioned getLatestVersion( @KeyValue T entity ) {
        if ( entity instanceof Versioned )
            return ( (Versioned) entity ).getLatestVersion();
        return null;
    }

    @SupplierKey(MODIFICATION_DATE_KEY)
    @KeyWeight(15)
    @API("1.0.0")
    public Date getModificationDate( @KeyValue SearchResult result ) {
        return result.getLastModificationDate();
    }

    @SupplierKey(MODIFICATION_DATE_KEY)
    @KeyWeight(10)
    @API("1.0.0")
    public Date getModificationDate( @KeyValue T entity ) {
        return entity.getLastModificationDate();
    }

    @SupplierKey(CREATION_DATE_KEY)
    @KeyWeight(25)
    @API("1.0.0")
    public Date getCreationDate( @KeyValue SearchResult result ) {
        return result.getCreationDate();
    }

    @SupplierKey(CREATION_DATE_KEY)
    @KeyWeight(20)
    @API("1.0.0")
    public Date getCreationDate( @KeyValue T entity ) {
        return entity.getCreationDate();
    }

    @SupplierKey(ID_KEY)
    @KeyWeight(5)
    @API("1.0.0")
    public long getId( @KeyValue T entity ) {
        return entity.getId();
    }

    @SupplierKey(ICON_KEY)
    @API("1.0.0")
    public String getIconURL( @KeyValue T entity ) {
        return findIconURL( entity );
    }

    protected abstract String findIconURL( T entity );

    /**
     * Returns the URL to access the specified entity.
     *
     * @param result search result value.
     * @return The web-context-relative URL.
     */
    @SupplierKey(URL_KEY)
    @KeyWeight(5)
    @API("1.0.0")
    public String getUrl( @KeyValue SearchResult result ) {
        return findUrl( result );
    }

    protected String findUrl( SearchResult result ) {
        return result.getUrlPath();
    }

    /**
     * Returns the URL to access the specified entity.
     *
     * @param entity The entity.
     * @return The web-context-relative URL.
     */
    @SupplierKey(URL_KEY)
    @API("1.0.0")
    public String getUrl( @KeyValue T entity ) {
        return findUrl( entity );
    }

    @API("1.0.0")
    protected abstract String findUrl( T entity );

    /**
     * Returns the human-readable title for the entity.
     *
     * @param result The search result value.
     * @return The title.
     */
    @SupplierKey(TITLE_KEY)
    @KeyWeight(105)
    @API("1.0.0")
    public String getTitle( @KeyValue SearchResult result ) {
        return result.getDisplayTitle();
    }

    /**
     * Returns the human-readable title for the entity.
     *
     * @param entity The entity.
     * @return The title.
     */
    @SupplierKey(TITLE_KEY)
    @KeyWeight(100)
    @API("1.0.0")
    public String getTitle( @KeyValue T entity ) {
        return findTitle( entity );
    }

    @API("1.0.0")
    protected abstract String findTitle( @KeyValue T entity );

    @SupplierKey(TYPE_KEY)
    @KeyWeight(5)
    @API("1.0.0")
    public String getType( @KeyValue SearchResult result ) {
        return result.getType();
    }

    /**
     * Returns the entity type string.
     *
     * @param entity The entity.
     * @return The type string.
     */
    @SupplierKey(TYPE_KEY)
    @API("1.0.0")
    public String getType( @KeyValue T entity ) {
        return findType( entity );
    }

    @API("1.0.0")
    protected abstract String findType( @KeyValue T entity );

    /**
     * Finds the URL which the specified value can link to for more information.
     * The URL may be relative to the Confluence install, or absolute. If none
     * is available, return <code>null</code>.
     *
     * @param context The context object.
     * @return The URL.
     * @throws SupplierException if there is a problem finding the value.
     */
    @API("1.0.0")
    public String getLink( SupplierContext context ) throws SupplierException {
        SearchResult result = context.getValueAs( SearchResult.class );
        if ( result != null )
            return findUrl( result );

        T entity = context.getValueAs( getEntityType() );
        if ( entity != null )
            return findUrl( entity );

        return null;
    }

    @SupplierKey("equals {other entity}")
    @API("1.0.0")
    public Boolean equalsKey( @KeyValue T entity, @KeyParam("other entity") String key, @KeyContext SupplierContext context ) {
        ConfluenceEntityObject target = context.getEnvironmentValue( ContentEntityObject.class );
        ConversionContext ctx = context.getEnvironmentValue( ConversionContext.class );

        key = key.trim();
        if ( target != null ) {
            if ( SELF.equals( key ) )
                return entity.equals( target );
            else if ( PARENT.equals( key ) && target instanceof Page )
                return entity.equals( ( (Page) target ).getParent() );

            target = linkAssistant.getEntityForWikiLink( ctx, key );
            return entity.equals( target );
        }
        return Boolean.FALSE;
    }

    private Object getUser( String username ) {
        if ( username != null ) {
            User user = getUserAccessor().getUser( username );
            if ( user != null )
                return user;
        }
        return username;
    }

    /**
     * Returns I18N text for the specified key.
     *
     * @param key The I18N key to retrieve.
     * @param def The default value to return if no value is found for the provided key.
     * @return The text value.
     */
    @API("1.0.0")
    protected String getText( String key, String def ) {
        if ( resources == null ) {
            resources = new ResourceStorage( getClass() );
        }

        return resources.getString( key, def );
    }

    @Autowired
    public void setLinkManager( LinkManager linkManager ) {
        this.linkManager = linkManager;
    }

    @API("1.0.0")
    protected LinkManager getLinkManager() {
        return linkManager;
    }

    @Autowired
    public void setLinkResolver( LinkResolver linkResolver ) {
        this.linkResolver = linkResolver;
    }

    @API("1.0.0")
    protected LinkResolver getLinkResolver() {
        return linkResolver;
    }

    @Autowired
    public void setLinkAssistant( LinkAssistant linkAssistant ) {
        this.linkAssistant = linkAssistant;
    }

    @API("1.0.0")
    protected LinkAssistant getLinkAssistant() {
        return linkAssistant;
    }

    /**
     * @return The specific entity type handled by this supplier.
     */
    @API("1.0.0")
    public Class<T> getEntityType() {
        return entityType;
    }

    @Override
    public String getDisplayText( SupplierContext context ) throws SupplierException {
        SearchResult result = context.getValueAs( SearchResult.class );
        if ( result != null )
            return getTitle( result );

        T entity = context.getValueAs( getEntityType() );
        if ( entity != null )
            return getTitle( entity );

        return null;
    }
}
