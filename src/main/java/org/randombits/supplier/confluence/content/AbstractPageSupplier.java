package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.TinyUrl;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.utils.lang.API;

import java.util.List;

@API("1.0.0")
public abstract class AbstractPageSupplier<T extends AbstractPage> extends AbstractSpaceContentSupplier<T> {

    private static final String TINY_URL_PREFIX = "/x/";

    @SupplierKey({"tinyurl", "tiny url"})
    @API("1.0.0")
    public String getTinyUrl(@KeyValue T page) {
        return TINY_URL_PREFIX + new TinyUrl(page).getIdentifier();
    }

    @SupplierKey({"is favourite", "is favorite"})
    @API("1.0.0")
    public boolean isFavourite(@KeyValue T page) {
        return page.isFavourite(getCurrentUser());
    }

    @SupplierKey("all comments")
    @API("1.0.0")
    public List<Comment> getAllComments(@KeyValue T page) {
        return page.getComments();
    }

    @SupplierKey("comments")
    @API("1.0.0")
    public List<Comment> getTopLevelComments(@KeyValue T page) {
        return page.getTopLevelComments();
    }

}
