/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.Namespace;
import com.atlassian.user.User;
import org.randombits.supplier.confluence.AbstractConfluenceSupplier;
import org.randombits.supplier.core.LinkableSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.core.annotate.SupportedTypes;
import org.randombits.utils.lang.API;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

/**
 * Supplies information about labels.
 *
 * @author David Peterson
 */
@SupplierPrefix("label")
@SupportedTypes(Label.class)
public class LabelSupplier extends AbstractConfluenceSupplier implements LinkableSupplier {

    @SupplierKey("creation date")
    @API("1.0.0")
    private Date getCreationDate( @KeyValue Label label ) {
        return label.getCreationDate();
    }

    @SupplierKey("is global")
    @API("1.0.0")
    private boolean isGlobalLabel( @KeyValue Label label ) {
        return Namespace.isGlobal( label );
    }

    @SupplierKey("is personal")
    @API("1.0.0")
    private boolean isPersonalLabel( @KeyValue Label label ) {
        return Namespace.isPersonal( label );
    }

    @SupplierKey("is team")
    @API("1.0.0")
    private boolean isTeamLabel( @KeyValue Label label ) {
        return Namespace.isTeam( label );
    }

    @SupplierKey("namespace")
    @API("1.0.0")
    private String getNamespace( @KeyValue Label label ) {
        return label.getNamespace().getPrefix();
    }

    @SupplierKey("content count")
    @API("1.0.0")
    private int getContentCount( @KeyValue Label label ) {
        return getLabelManager().getContentCount( label );
    }

    @SupplierKey("owner")
    @API("1.0.0")
    private User getOwner( @KeyValue Label label ) {
        return getUser( label.getOwner() );
    }

    @SupplierKey({"name", "title"})
    @API("1.0.0")
    private String getName( @KeyValue Label label ) {
        return label.getDisplayTitle();
    }

    @SuppressWarnings({"unchecked"})
    @SupplierKey("content")
    @API("1.0.0")
    private List<ContentEntityObject> getLabelContent( @KeyValue Label label ) {
        return getPermittedObjects( getLabelManager().getContent( label ) );
    }

    @SuppressWarnings({"unchecked"})
    @SupplierKey("related")
    @API("1.0.0")
    private List<Label> getRelatedLabels( @KeyValue Label label ) {
        List<Label> related = getLabelManager().getRelatedLabels( label );
        return getPermittedLabels( related );
    }

    @SupplierKey("url")
    @API("1.0.0")
    private String getLabelURL( @KeyValue Label label ) {
        StringBuilder out = new StringBuilder();
        out.append( "/label/" );
        if ( !isGlobalLabel( label ) )
            out.append( urlEncode( label.getNamespace().getPrefix() ) ).append( ":" );
        out.append( urlEncode( label.getName() ) );
        // out.append( label.toString() );

        return out.toString();
    }

    private String urlEncode( String text ) {
        try {
            return URLEncoder.encode( text, "UTF-8" );
        } catch ( UnsupportedEncodingException e ) {
            // This _should_ never happen. Just return the original text.
            return text;
        }
    }

    private User getUser( String name ) {
        if ( name != null )
            return getUserAccessor().getUser( name );

        return null;
    }

    /**
     * Finds the URL which the specified value can link to for more information.
     * The URL may a server-relative link, or absolute. If none is available,
     * return <code>null</code>.
     *
     * @param context The context object.
     * @return The URL.
     * @throws SupplierException if there is a problem finding the value.
     */
    @Override
    public String getLink( SupplierContext context ) throws SupplierException {
        Label label = context.getValueAs( Label.class );
        return label == null ? null : getLabelURL( label );
    }

}
