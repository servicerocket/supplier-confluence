package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.spaces.Space;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@SupplierPrefix("attachment")
@SupportedTypes(Attachment.class)
@API("1.0.0")
public class AttachmentSupplier extends AbstractEntitySupplier<Attachment> {

    private static final String ATTACHMENT_DOWNLOAD_PATH = "attachmentDownloadPath";

    private static final String ATTACHMENT_MIME_TYPE = "attachment-mime-type";

    private static final String NICE_FILE_SIZE = "niceFileSize";

    private static final String THUMBNAIL_PATH = "thumbUrl";

    private AttachmentManager attachmentManager;

    /**
     * Constructs an annotated supplier.
     */
    public AttachmentSupplier() {
    }

    @Override
    protected String getResultType() {
        return Attachment.CONTENT_TYPE;
    }

    @Override
    protected String findIconURL(Attachment attachment) {
        String icon;
        String fileName = attachment.getFileName();
        String contentType = attachment.getContentType();

        if (fileName.endsWith(".pdf") || contentType.equals("application/pdf"))
            icon = "pdf.gif";
        else if (contentType.startsWith("image/"))
            icon = "image.gif";
        else if (contentType.startsWith("text/xml"))
            icon = "xml.gif";
        else if (contentType.startsWith("text/html"))
            icon = "html.gif";
        else if (fileName.endsWith(".java") || fileName.endsWith(".jar"))
            icon = "java.gif";
        else if (contentType.startsWith("text/plain"))
            icon = "text.gif";
        else if (contentType.startsWith("application") && contentType.contains("zip"))
            icon = "zip.gif";
        else if (fileName.endsWith(".xls") || contentType.startsWith("application/vnd.ms-excel"))
            icon = "excel.gif";
        else if (fileName.endsWith(".ppt") || contentType.startsWith("application/vnd.ms-powerpoint"))
            icon = "powerpoint.gif";
        else if (fileName.endsWith(".doc") || contentType.startsWith("application/msword"))
            icon = "word.gif";
        else
            icon = "file.gif";

        return "/images/icons/attachments/" + icon;
    }

    @Override
    protected String findTitle(Attachment attachment) {
        return attachment.getFileName();
    }

    @Override
    protected String findType(Attachment attachment) {
        String type = attachment.getType().toLowerCase();
        return getText(type + ".type", type);
    }

    @Override
    protected String findUrl(Attachment attachment) {
        return getDownloadPath(attachment);
    }

    @Override
    public String findUrl(SearchResult result) {
        return result.getExtraFields().get(ATTACHMENT_DOWNLOAD_PATH);
    }


    @SupplierKey("space")
    @API("1.0.0")
    public Space getSpace(@KeyValue Attachment attachment) {
        return getSpace(attachment.getContent());
    }

    @SupplierKey("attachment version")
    @KeyWeight(5)
    @API("1.0.0")
    public Integer getAttachmentVersion(@KeyValue SearchResult result) {
        return result.getContentVersion();
    }

    @SupplierKey("attachment version")
    @API("1.0.0")
    public Integer getAttachmentVersion(@KeyValue Attachment attachment) {
        return attachment.getAttachmentVersion();
    }

    @SupplierKey("attached to")
    @API("1.0.0")
    public ContentEntityObject getAttachedTo(@KeyValue Attachment attachment) {
        return attachment.getContent();
    }

    @SupplierKey("comment")
    @API("1.0.0")
    public String getComment(@KeyValue Attachment attachment) {
        return attachment.getComment();
    }

    @SupplierKey("content type")
    @KeyWeight(5)
    @API("1.0.0")
    public String getContentType(@KeyValue SearchResult result) {
        return result.getField(ATTACHMENT_MIME_TYPE);
    }

    @SupplierKey("content type")
    @API("1.0.0")
    public String getContentType(@KeyValue Attachment attachment) {
        return attachment.getContentType();
    }

    @SupplierKey("nice file size")
    @KeyWeight(5)
    @API("1.0.0")
    public String getNiceFileSizeKey(@KeyValue SearchResult result) {
        return result.getField(NICE_FILE_SIZE);
    }

    @SupplierKey("nice file size")
    @API("1.0.0")
    public String getNiceFileSize(@KeyValue Attachment attachment) {
        return attachment.getNiceFileSize();
    }

    @SupplierKey("file size")
    @API("1.0.0")
    public Long getFileSize(@KeyValue Attachment attachment) {
        return attachment.getFileSize();
    }

    @SupplierKey("file name")
    @API("1.0.0")
    public String getFileName(@KeyValue Attachment attachment) {
        return attachment.getFileName();
    }

    @SupplierKey("version history")
    @API("1.0.0")
    public List<Attachment> getVersionHistory(@KeyValue Attachment attachment) {
        return attachmentManager.getAllVersions(attachment);
    }

    @SupplierKey("thumbnail path")
    @KeyWeight(5)
    @API("1.0.0")
    public String getThumbnailPath(@KeyValue SearchResult result) {
        return result.getField(THUMBNAIL_PATH);
    }

    @SupplierKey("thumbnail path")
    @API("1.0.0")
    public String getThumbnailPath(@KeyValue Attachment attachment) {
        String downloadPath = getDownloadPath(attachment);
        if (downloadPath != null)
            downloadPath = downloadPath.replaceAll("/attachments/", "/thumbnails/");
        return downloadPath;
    }

    @SupplierKey("download path")
    @KeyWeight(5)
    @API("1.0.0")
    public String getDownloadPath(@KeyValue SearchResult result) {
        return getUrl(result);
    }

    @SupplierKey("download path")
    @API("1.0.0")
    public String getDownloadPath(@KeyValue Attachment attachment) {
        return attachment.getDownloadPath();
    }

    private Space getSpace(ContentEntityObject content) {
        if (content instanceof SpaceContentEntityObject)
            return ((SpaceContentEntityObject) content).getSpace();

        return null;
    }

    @Autowired
    public void setAttachmentManager(AttachmentManager attachmentManager) {
        this.attachmentManager = attachmentManager;
    }

    @API("1.0.0")
    protected AttachmentManager getAttachmentManager() {
        return attachmentManager;
    }
}
