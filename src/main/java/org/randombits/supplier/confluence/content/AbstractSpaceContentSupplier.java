package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.spaces.Space;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.utils.lang.API;

@API("1.0.0")
public abstract class AbstractSpaceContentSupplier<T extends SpaceContentEntityObject> extends AbstractContentSupplier<T> {

    private static final String SPACE_KEY = "space";

    /**
     * Constructs an annotated supplier.
     */
    public AbstractSpaceContentSupplier() {
    }

    @SupplierKey(SPACE_KEY)
    @API("1.0.0")
    public Space getSpaceKey(@KeyValue T entity) {
        return entity.getSpace();
    }
}
