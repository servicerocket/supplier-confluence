/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.links.OutgoingLink;
import com.atlassian.confluence.links.linktypes.PageCreateLink;
import com.atlassian.confluence.mail.Mail;
import com.atlassian.confluence.mail.MailContentManager;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.*;
import com.atlassian.confluence.search.v2.lucene.LuceneSearchResults;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.TitleSort;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.themes.ThemeManager;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.renderer.links.Link;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;
import org.randombits.supplier.core.LinkableSupplier;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.core.annotate.SupportedTypes;
import org.randombits.support.core.text.I18NAssistant;
import org.randombits.utils.lang.API;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Supplies information about Confluence Spaces.
 *
 * @author David Peterson
 */
@SupplierPrefix("space")
@SupportedTypes(Space.class)
public class SpaceSupplier extends AbstractEntitySupplier<Space> implements LinkableSupplier {

    private PageManager pageManager;

    private MailContentManager mailContentManager;

    private ThemeManager themeManager;

    private PluginAccessor pluginAccessor;

    private I18NAssistant i18NAssistant;

    private SearchManager searchManager;

    @SupplierKey("space type")
    @API("1.0.0")
    public String getSpaceType(@KeyValue Space space) {
        return getText("spacetype." + space.getSpaceType(), space.getSpaceType().toString());
    }

     /**
     * <p>FIXME [AA 20130502] PageTemplate is commented from List&lt;PageTemplate&gt; simply to avoid CNFE while this bean is constructed.
     * See <a href="https://service.customware.net/jira/browse/AN21-108">AN21-108</a></p>
     * 
     * @param space
     * @return 
     */
    @SuppressWarnings("unchecked")
    @SupplierKey("page templates")
    @API("1.0.0")
    public List/*<PageTemplate>*/ getPageTemplates(@KeyValue Space space) {
        return space.getPageTemplates();
    }

    @SupplierKey("is personal")
    @API("1.0.0")
    public boolean isPersonal(@KeyValue Space space) {
        return space.isPersonal();
    }

    @SupplierKey("is global")
    @API("1.0.0")
    public boolean isGlobal(@KeyValue Space space) {
        return space.isGlobal();
    }

    @SupplierKey("description")
    @API("1.0.0")
    public static SpaceDescription getDescription(@KeyValue Space space) {
        return space.getDescription();
    }

    @SupplierKey({"homepage", "home page"})
    @API("1.0.0")
    public Page getHomePage(@KeyValue Space space) {
        return space.getHomePage();
    }

    @SupplierKey("key")
    @API("1.0.0")
    public String getKey(@KeyValue Space space) {
        return space.getKey();
    }

    @SupplierKey({"name", "title"})
    @API("1.0.0")
    public String getName(@KeyValue Space space) {
        return space.getName();
    }

    @SupplierKey("theme name")
    @API("1.0.0")
    public String getThemeName(@KeyValue Space space) {
        String themeName;

        String themeKey = getThemeKey(space);
        if (StringUtils.isBlank(themeKey)) {
            Plugin plugin = getPluginAccessor().getPlugin(getThemeManager().getGlobalTheme().getPluginKey());
            themeName = getPluginName(plugin);
        } else {
            ModuleDescriptor<?> module = getPluginAccessor().getPluginModule(themeKey);
            if (module == null)
                return null;
            else {
                themeName = getI18nName(module.getName(), module.getI18nNameKey());
            }

            if (themeName == null) {
                themeName = getPluginName(module.getPlugin());
            }
        }

        return themeName;
    }

    private String getPluginName(Plugin plugin) {
        if (plugin != null)
            return getI18nName(plugin.getName(), plugin.getI18nNameKey());
        return null;
    }

    private String getI18nName(String name, String i18nKey) {
        String i18nName = null;

        if (StringUtils.isNotBlank(i18nKey))
            i18nName = i18NAssistant.getText(i18nKey);

        if (i18nName == null || i18nName.equals(i18nKey))
            i18nName = name;

        return i18nName;
    }

    @SupplierKey("theme key")
    private String getThemeKey(@KeyValue Space space) {
        return getThemeManager().getSpaceThemeKey(space.getKey());
    }

    @SuppressWarnings("unchecked")
    @SupplierKey("mail")
    @API("1.0.0")
    public List<Mail> getMail(@KeyValue Space space) {
        return getMailContentManager().getMail(space, true);
    }

    @SupplierKey({"is favorite", "is favourite"})
    @API("1.0.0")
    public static Boolean isUserFavourite(@KeyValue Space space) {
        User user = getCurrentUser();
        return getDescription(space).isFavourite(user);
    }

    @SuppressWarnings("unchecked")
    @SupplierKey({"labels", "categories"})
    @API("1.0.0")
    public static List<Label> getLabels(@KeyValue Space space) {
        SpaceDescription desc = getDescription(space);
        if (desc != null)
            return desc.getLabelsForDisplay(getCurrentUser());
        return null;
    }

    @SuppressWarnings("unchecked")
    @SupplierKey({"news", "blog posts"})
    @API("1.0.0")
    public List<BlogPost> getBlogPosts(@KeyValue Space space) {
        return getPermittedObjects(getPageManager().getBlogPosts(space, true));
    }

    @SuppressWarnings("unchecked")
    @SupplierKey("labels in content")
    @API("1.0.0")
    public List<Label> getLabelsInContent(@KeyValue Space space) {
        return getPermittedLabels(getLabelManager().getLabelsInSpace(space.getKey()));
    }

    @SuppressWarnings("unchecked")
    @SupplierKey("all pages")
    @API("1.0.0")
    public List<Page> getAllPages(@KeyValue Space space) {
        return getPermittedObjects(getPageManager().getPages(space, true));
    }

    @SuppressWarnings("unchecked")
    @SupplierKey({"pages", "root pages"})
    @API("1.0.0")
    public List<Page> getRootPages(@KeyValue Space space) {
        return getPermittedObjects(getPageManager().getTopLevelPages(space));
    }

    @SuppressWarnings("unchecked")
    @SupplierKey("orphaned pages")
    @API("1.0.0")
    public List<Page> getOrphanedPages(@KeyValue Space space) {
        return getPermittedObjects(getPageManager().getOrphanedPages(space.getKey()));
    }

    @SupplierKey({"undefined links", "undefined pages"})
    @API("1.0.0")
    public Set<Link> getUndefinedLinks(@KeyValue Space space) {
        Set<Link> links = new HashSet<Link>();

        @SuppressWarnings("unchecked")
        List<OutgoingLink> pages = getPageManager().getUndefinedPages(space.getKey());
        Link link;
        String linkText;

        PageContext ctx = new PageContext();

        for (OutgoingLink outLink : pages) {
            linkText = outLink.getDestinationSpaceKey() + ":" + outLink.getDestinationPageTitle();

            if (outLink.getLinkTitle() != null)
                linkText = outLink.getLinkTitle() + "|" + linkText;

            link = getLinkResolver().createLink(ctx, linkText);

            if (link instanceof PageCreateLink)
                links.add(link);
        }

        return links;
    }

    /**
     * Returns the list of attachments as a set of SearchResults.
     *
     * @param space The space to search on.
     * @return The set of results.
     */
    @SupplierKey("attachments")
    @API("1.0.0")
    public SearchResults getAttachments(@KeyValue Space space) {
        SearchQuery query = BooleanQuery.composeAndQuery(new InSpaceQuery(space.getKey()), new ContentTypeQuery(ContentTypeEnum.ATTACHMENT));
        SearchSort sort = new TitleSort(SearchSort.Order.DESCENDING);
        SearchFilter securityFilter = SiteSearchPermissionsSearchFilter.getInstance();

        ContentSearch search = new ContentSearch(query, sort, securityFilter, null);

        SearchResults searchResults;
        try {
            searchResults = searchManager.search(search);
        } catch (InvalidSearchException e) {
            // discard search and assign empty results
            searchResults = LuceneSearchResults.EMPTY_RESULTS;
        }

        return searchResults;
    }

    @Autowired
    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    @Autowired
    public void setMailContentManager(MailContentManager mailContentManager) {
        this.mailContentManager = mailContentManager;
    }

    @Autowired
    public void setThemeManager(ThemeManager themeManager) {
        this.themeManager = themeManager;
    }

    @Autowired
    public void setPluginAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    @Autowired
    public void setI18NAssistant(I18NAssistant i18NAssistant) {
        this.i18NAssistant = i18NAssistant;
    }

    @Autowired
    public void setSearchManager(SearchManager searchManager) {
        this.searchManager = searchManager;
    }

    private PageManager getPageManager() {
        return pageManager;
    }

    private MailContentManager getMailContentManager() {
        return mailContentManager;
    }

    private ThemeManager getThemeManager() {
        return themeManager;
    }

    private PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    @Override
    protected String findType(Space space) {
        return getText("space.type", "space");
    }

    @Override
    protected String getResultType() {
        return Space.CONTENT_TYPE;
    }

    @Override
    protected String findIconURL(Space space) {
        return "/images/icons/browse_space.gif";
    }

    @Override
    protected String findTitle(Space entity) {
        return entity.getName();
    }

    @Override
    protected String findUrl(Space entity) {
        return entity.getUrlPath();
    }
}
