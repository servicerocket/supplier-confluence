package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.core.BodyContent;
import com.atlassian.confluence.core.ContentEntityObject;
import org.randombits.supplier.core.DisplayableSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;

/**
 * Supplies data about the {@link BodyContent} for a {@link com.atlassian.confluence.core.ContentEntityObject}.
 */
@SupplierPrefix("body")
@SupportedTypes(BodyContent.class)
public class BodyContentSupplier extends AnnotatedSupplier implements DisplayableSupplier {

    @SupplierKey("text")
    public String getText( @KeyValue BodyContent body ) {
        return body.getBody();
    }

    @SupplierKey("type")
    public String getType( @KeyValue BodyContent body ) {
        return body.getBodyType().toString();
    }

    @SupplierKey("content")
    public ContentEntityObject getContent( @KeyValue BodyContent body ) {
        return body.getContent();
    }

    @SupplierKey("id")
    public long getId( @KeyValue BodyContent body ) {
        return body.getId();
    }

    @Override
    public String getDisplayText( SupplierContext context ) throws SupplierException {
        BodyContent body = context.getValueAs( BodyContent.class );
        return body != null ? body.getBody() : null;
    }
}
