package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.spaces.Space;
import org.randombits.supplier.core.HierarchicalSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.core.annotate.SupportedTypes;
import org.randombits.utils.lang.API;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@SupplierPrefix("comment")
@SupportedTypes(Comment.class)
public class CommentSupplier extends AbstractContentSupplier<Comment> implements HierarchicalSupplier {

    @SupplierKey("space")
    @API("1.0.0")
    private Space getSpace( @KeyValue Comment comment ) {
        if ( comment.getOwner() instanceof SpaceContentEntityObject ) {
            SpaceContentEntityObject owner = (SpaceContentEntityObject) comment.getOwner();
            return owner.getSpace();
        }
        return null;
    }

    @SupplierKey({"page", "owner"})
    @API("1.0.0")
    private ContentEntityObject getOwner( @KeyValue Comment comment ) {
        return comment.getOwner();
    }

    @SupplierKey({"parent", "replying to"})
    @API("1.0.0")
    private Comment getParent( @KeyValue Comment comment ) {
        return comment.getParent();
    }

    @SupplierKey("reply count")
    @API("1.0.0")
    private int getReplyCount( @KeyValue Comment comment ) {
        return comment.getDescendantsCount();
    }

    @SupplierKey("repliers")
    @API("1.0.0")
    private Set<String> getRepliers( @KeyValue Comment comment ) {
        return comment.getDescendantAuthors();
    }

    @SupplierKey("depth")
    @API("1.0.0")
    private int getDepth( @KeyValue Comment comment ) {
        return comment.getDepth();
    }

    @SupplierKey({"replies", "children"})
    @API("1.0.0")
    private List<Comment> getReplies( @KeyValue Comment comment ) {
        return comment.getChildren();
    }

    @Override
    public Collection<?> getChildren( SupplierContext context ) throws SupplierException {
        Comment comment = context.getValueAs( Comment.class );
        return comment == null ? null : getReplies( comment );
    }

    @Override
    public Object getParent( SupplierContext context ) throws SupplierException {
        Comment comment = context.getValueAs( Comment.class );
        return comment == null ? null : getParent( comment );
    }

    @Override
    protected String getResultType() {
        return Comment.CONTENT_TYPE;
    }

    @Override
    public String findTitle( Comment comment ) {
        return comment.getDisplayTitle();
    }
}
