package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.pages.Page;
import com.atlassian.user.User;
import org.randombits.supplier.core.HierarchicalSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.support.confluence.ContextAssistant;
import org.randombits.utils.lang.API;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@SupplierPrefix("page")
@SupportedTypes(Page.class)
public class PageSupplier extends AbstractPageSupplier<Page> implements HierarchicalSupplier {

    public static final String PAGE_PREFIX = "page";

    private static final String PARENT_KEY = "parent";

    private static final String CHILDREN_KEY = "children";

    private static final String ANCESTORS_KEY = "ancestors";

    private static final String DESCENDENTS_KEY = "descendents";

    private static final String DESCENDANTS_KEY = "descendants";

    private static final String SORTED_CHILDREN_KEY = "sorted children";

    private static final String SORTED_DESCENDENTS_KEY = "sorted descendents";

    private static final String SORTED_DESCENDANTS_KEY = "sorted descendants";

    private ContextAssistant contextAssistant;

    public PageSupplier() {
    }

    @SupplierKey(SORTED_CHILDREN_KEY)
    @API("1.0.0")
    private List<Page> getSortedChildren( @KeyValue Page page ) {
        return getPermittedObjects( page.getSortedChildren() );
    }

    @SupplierKey({SORTED_DESCENDENTS_KEY, SORTED_DESCENDANTS_KEY})
    @API("1.0.0")
    private List<Page> getSortedDescendants( @KeyValue Page page ) {
        List<Page> descendants = new ArrayList<Page>();

        addSortedDescendants( page, descendants );

        return descendants;
    }

    private void addSortedDescendants( Page page, List<Page> descendants ) {
        for ( Page child : page.getSortedChildren() ) {
            descendants.add( child );
            addSortedDescendants( child, descendants );
        }
    }

    @SupplierKey({DESCENDENTS_KEY, DESCENDANTS_KEY})
    @KeyWeight(50)
    @API("1.0.0")
    private List<Page> getDescendants( @KeyValue Page page ) {
        return getPermittedObjects( page.getDescendents() );
    }

    @SupplierKey(CHILDREN_KEY)
    @KeyWeight(100)
    @API("1.0.0")
    private List<Page> getChildren( @KeyValue Page page ) {
        return getPermittedObjects( page.getChildren() );
    }

    @SupplierKey(ANCESTORS_KEY)
    @API("1.0.0")
    private List<Page> getAncestors( @KeyValue Page page ) {
        return page.getAncestors();
    }

    @SupplierKey(PARENT_KEY)
    @KeyWeight(50)
    @API("1.0.0")
    private Page getParent( @KeyValue Page page ) {
        return page.getParent();
    }

    @Override
    protected String findIconFileName( Page page ) {
        if ( page.isHomePage() )
            return getText( "homepage.icon", null );
        else {
            User user = getCurrentUser();
            if ( user != null && contextAssistant.isRecentlyUpdatedFor( page, user ) )
                return getText( "newpage.icon", null );
            else
                return getText( "page.icon", null );
        }

    }

    @Override
    public Collection<?> getChildren( SupplierContext context ) throws SupplierException {
        Page page = context.getValueAs( Page.class );
        return page == null ? null : getSortedChildren( page );
    }

    @Override
    public Object getParent( SupplierContext context ) throws SupplierException {
        Page page = context.getValueAs( Page.class );
        return page == null ? null : page.getParent();
    }

    @Autowired
    public void setContextAssistant( ContextAssistant contextAssistant ) {
        this.contextAssistant = contextAssistant;
    }

    @Override
    protected String getResultType() {
        return Page.CONTENT_TYPE;
    }
}
