/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.confluence.content;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.links.linktypes.*;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.UnresolvedLink;
import com.atlassian.renderer.links.UrlLink;
import org.randombits.supplier.core.LinkableSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

import java.util.Map;

/**
 * Supplies information about links.
 *
 * @author David Peterson
 */
@SupplierPrefix("link")
@SupportedTypes(Link.class)
@API("1.0.0")
public class LinkSupplier extends AnnotatedSupplier implements LinkableSupplier {

    private static final String MAILTO_PREFIX = "mailto:";

    private static final Map<Class<? extends Link>, String> TYPES = new java.util.HashMap<Class<? extends Link>, String>();

    static {
        TYPES.put( PageLink.class, "Page" );
        TYPES.put( UrlLink.class, "URL" );
        TYPES.put( UnresolvedLink.class, "Unresolved" );
        TYPES.put( PageCreateLink.class, "Page Create" );
        TYPES.put( BlogPostLink.class, "News Item" );
        TYPES.put( ContentLink.class, "Content" );
        TYPES.put( SpaceLink.class, "Space" );
        TYPES.put( UserProfileLink.class, "User Profile" );
    }

    public Object getDefaultValue( Link link ) {
        if ( link instanceof PageCreateLink ) {
            return ( (PageCreateLink) link ).getPageTitle();
        }
        if ( link instanceof AbstractContentEntityLink ) {
            // TODO: Really?  Return the destination content object?
            AbstractContentEntityLink contentLink = (AbstractContentEntityLink) link;
            if ( contentLink.hasDestination() )
                return getDestination( contentLink );
        }
        return getBody( link );
    }

    private String getUrlLinkUrl( Link link ) {
        if ( link instanceof UrlLink ) {
            UrlLink urlLink = (UrlLink) link;
            return urlLink.getUrl().toLowerCase();
        }
        return null;
    }

    @SupplierKey("is mail link")
    @API("1.0.0")
    public boolean isMailLink( @KeyValue Link link ) {
        String url = getUrlLinkUrl( link );
        return url != null && url.startsWith( MAILTO_PREFIX );
    }

    @SupplierKey("is external")
    @API("1.0.0")
    public boolean isExternal( @KeyValue Link link ) {
        String url = getUrlLinkUrl( link );
        return url != null && !url.startsWith( MAILTO_PREFIX );
    }


    @SupplierKey("info")
    @API("1.0.0")
    public String getInfo( @KeyValue Link link ) {
        return link.getTitle();
    }

    @SupplierKey("body")
    @API("1.0.0")
    public String getBody( @KeyValue Link link ) {
        return link.getLinkBody();
    }

    @SupplierKey("has destination")
    @API("1.0.0")
    public boolean hasDestination( @KeyValue Link link ) {
        if ( link instanceof AbstractContentEntityLink ) {
            AbstractContentEntityLink contentLink = (AbstractContentEntityLink) link;
            if ( contentLink.hasDestination() ) {
                return contentLink.hasDestination();
            }
        }
        return false;
    }

    @SupplierKey("destination")
    @API("1.0.0")
    public ContentEntityObject getDestination( @KeyValue Link link ) {
        if ( link instanceof AbstractContentEntityLink ) {
            AbstractContentEntityLink contentLink = (AbstractContentEntityLink) link;
            if ( contentLink.hasDestination() )
                return contentLink.getDestinationContent();
        }
        return null;
    }

    @SupplierKey("type")
    @API("1.0.0")
    public String getLinkType( @KeyValue Link link ) {
        return TYPES.get( link.getClass() );
    }

    @SupplierKey("url")
    private String getLinkUrl( @KeyValue Link link ) {
        return link.getUrl();
    }

    @Override
    public String getLink( SupplierContext context ) throws SupplierException {
        Link link = context.getValueAs( Link.class );
        return link == null ? null : getLinkUrl( link );
    }
}
