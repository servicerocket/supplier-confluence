/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.confluence.user;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.user.Group;
import com.atlassian.user.User;
import com.atlassian.user.search.page.Pager;
import org.randombits.supplier.confluence.AbstractConfluenceSupplier;
import org.randombits.supplier.core.DisplayableSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.core.annotate.SupportedTypes;
import org.randombits.utils.lang.API;

import java.util.List;

/**
 * Supplies information about user groups
 *
 * @author David Peterson
 */
@SupplierPrefix("user-group")
@SupportedTypes(Group.class)
@API("1.0.0")
public class UserGroupSupplier extends AbstractConfluenceSupplier implements DisplayableSupplier {

    @SupplierKey("name")
    @API("1.0.0")
    public String getName( @KeyValue Group group ) {
        return group.getName();
    }

    @SupplierKey("members")
    @API("1.0.0")
    public List<User> getGroupMembers( @KeyValue Group group ) {
        if ( getPermissionManager().hasPermission( getCurrentUser(), Permission.ADMINISTER,
                PermissionManager.TARGET_APPLICATION ) ) {
            List<User> members = new java.util.ArrayList<User>();

            Pager<String> memberNames = getUserAccessor().getMemberNames( group );
            User user;

            for ( String name : memberNames ) {
                user = getUserAccessor().getUser( name );
                if ( user != null )
                    members.add( user );
            }

            return members;
        }
        return null;
    }

    @Override
    public String getDisplayText( SupplierContext context ) throws SupplierException {
        Group group = context.getValueAs( Group.class );
        return group == null ? null : getName( group );
    }
}
