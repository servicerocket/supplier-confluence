/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.confluence.user;

import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.persistence.ContentEntityObjectDao;
import com.atlassian.confluence.labels.*;
import com.atlassian.confluence.mail.address.ConfluenceMailAddress;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.login.LoginInfo;
import com.atlassian.confluence.security.login.LoginManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.*;
import com.atlassian.confluence.user.actions.ProfilePictureInfo;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.User;
import com.atlassian.user.search.page.Pager;
import com.opensymphony.module.propertyset.PropertyException;
import com.opensymphony.module.propertyset.PropertySet;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.iterators.FilterIterator;
import org.apache.log4j.Logger;
import org.randombits.supplier.confluence.AbstractConfluenceSupplier;
import org.randombits.supplier.core.DisplayableSupplier;
import org.randombits.supplier.core.LinkableSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.KeyValue;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.core.annotate.SupportedTypes;
import org.randombits.utils.lang.API;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Provides information about users.
 *
 * @author David Peterson
 */
@SupplierPrefix("user")
@SupportedTypes(User.class)
public class UserSupplier extends AbstractConfluenceSupplier implements LinkableSupplier, DisplayableSupplier {

    private static final Logger LOG = Logger.getLogger(UserSupplier.class);

    private ContentEntityManager contentEntityManager;

    private ContentEntityObjectDao contentEntityObjectDao;

    private PageManager pageManager;

    private PersonalInformationManager personalInformationManager;

    private SettingsManager settingsManager;

    private SpaceManager spaceManager;

    private LoginManager loginManager;

    private UserDetailsManager userDetailsManager;

    @Autowired
    public void setContentEntityManager(@Qualifier("contentEntityManager") ContentEntityManager contentEntityManager) {
        this.contentEntityManager = contentEntityManager;
    }

    @Autowired
    public void setContentEntityObjectDao(ContentEntityObjectDao contentEntityObjectDao) {
        this.contentEntityObjectDao = contentEntityObjectDao;
    }

    @Autowired
    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    @Autowired
    public void setPersonalInformationManager(PersonalInformationManager personalInformationManager) {
        this.personalInformationManager = personalInformationManager;
    }

    @Autowired
    public void setSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    @Autowired
    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    @Autowired
    public void setLoginManager(LoginManager loginManager) {
        this.loginManager = loginManager;
    }

    @Autowired
    public void setUserDetailsManager(UserDetailsManager userDetailsManager) {
        this.userDetailsManager = userDetailsManager;
    }

    @SupplierKey("personal space")
    @API("1.0.0")
    public Space getPersonalSpace(@KeyValue User user) {
        return getSpaceManager().getPersonalSpace(user);
    }

    @SupplierKey("is deactivated")
    @API("1.0.0")
    public boolean isDeactivated(@KeyValue User user) {
        return getUserAccessor().isDeactivated(user);
    }

    @SupplierKey({"name", "username"})
    @API("1.0.0")
    public String getName(@KeyValue User user) {
        return user.getName();
    }

    @SupplierKey("full name")
    @API("1.0.0")
    public String getFullName(@KeyValue User user) {
        return user.getFullName();
    }

    @SupplierKey("authored pages count")
    @API("1.0.0")
    public Integer getAuthoredPagesCount(@KeyValue User user) {
        if (getPageManager() != null)
            return getPageManager().getAuthoredPagesCountByUser(user.getName());

        return null;
    }

    @SuppressWarnings("unchecked")
    @SupplierKey("recently modified content")
    @API("1.0.0")
    public Iterator<ContentEntityObject> getRecentlyModifiedContent(@KeyValue User user) {
        Iterator<ContentEntityObject> content = contentEntityManager.getRecentlyModifiedEntitiesForUser(user
                .getName());
        final User currentUser = AuthenticatedUserThreadLocal.getUser();

        return new FilterIterator(content, new Predicate() {
            public boolean evaluate(Object value) {
                return getPermissionManager().hasPermission(currentUser, Permission.VIEW, value);
            }
        });
    }

    @SupplierKey("signup date")
    @API("1.0.0")
    public Date getSignupDate(@KeyValue User user) {
        try {
            return getDateProperty(user, UserPreferencesKeys.PROPERTY_USER_SIGNUP_DATE);
        } catch (Exception e) {
            LOG.warn("An error occurred trying to retrieve signup date: " + e, e);
        }

        return null;
    }

    @SupplierKey("last login date")
    @API("1.0.0")
    public Date getLastLoginDate(@KeyValue User user) {
        LoginInfo info = loginManager.getLoginInfo(user.getName());
        if (info != null) {
            return info.getLastSuccessfulLoginDate();
        }
        return null;
    }

    @SupplierKey("previous login date")
    @API("1.0.0")
    public Date getPreviousLoginDate(@KeyValue User user) {
        LoginInfo info = loginManager.getLoginInfo(user.getName());
        if (info != null) {
            return info.getPreviousSuccessfulLoginDate();
        }
        return null;
    }

    @SupplierKey("last failed login date")
    @API("1.0.0")
    public Date getLastFailedLoginDate(@KeyValue User user) {
        LoginInfo info = loginManager.getLoginInfo(user.getName());
        if (info != null) {
            return info.getLastFailedLoginDate();
        }
        return null;
    }

    @SupplierKey("current failed login count")
    @API("1.0.0")
    public Integer getCurrentFailedLoginCount(@KeyValue User user) {
        LoginInfo info = loginManager.getLoginInfo(user.getName());
        if (info != null) {
            return info.getCurrentFailedLoginCount();
        }
        return null;
    }

    @SupplierKey("total failed login count")
    @API("1.0.0")
    public Integer getTotalFailedLoginCount(@KeyValue User user) {
        LoginInfo info = loginManager.getLoginInfo(user.getName());
        if (info != null) {
            return info.getTotalFailedLoginCount();
        }
        return null;
    }

    /**
     * A backward compatibility method <p/> 'signup' and 'last login' date
     * properties are now actual Date objects but were stored as longs before.
     * This method checks the type of the property before fetching the value
     * with the appropriate getter method
     *
     * @param user        The user.
     * @param propertyKey The property key.
     * @return The date property, if set.
     */
    private Date getDateProperty(User user, String propertyKey) {
        PropertySet propertySet = getUserAccessor().getPropertySet(user);
        if (propertySet.exists(propertyKey)) {
            try {
                long aLong = propertySet.getLong(propertyKey);
                if (aLong > 0)
                    return new Date(aLong);
                else
                    // really ugly, but no way around it.
                    // getPropertySet().getType(key) is broken, so we can't
                    // check for types
                    return propertySet.getDate(propertyKey);
            } catch (PropertyException e) {
                return propertySet.getDate(propertyKey);
            }
        } else
            return null;
    }

    @SupplierKey({"favorite content", "favourite content"})
    @API("1.0.0")
    public List<ContentEntityObject> getFavouriteContent(@KeyValue User user) {
        if (isCurrentUser(user) || isGlobalAdministrator(getCurrentUser())) {
            Label favourite = getFavouriteLabel(user);
            if (favourite != null) {
                List<? extends Labelable> contents = getLabelManager().getCurrentContentForLabel(favourite);
                // Ugly way to switch wildcard to ContentEntityObject
                @SuppressWarnings("unchecked")
                List<ContentEntityObject> contentsAsEo = (List<ContentEntityObject>) contents;
                contentsAsEo = removeSpaceDescriptions(contentsAsEo);
                return getPermissionManager().getPermittedEntities(getCurrentUser(), Permission.VIEW, contentsAsEo);
            }
        }
        return null;
    }

    private List<ContentEntityObject> removeSpaceDescriptions(List<ContentEntityObject> contents) {
        Iterator<ContentEntityObject> i = contents.iterator();

        while (i.hasNext()) {
            Object object = i.next();
            if (object instanceof SpaceDescription)
                i.remove();
        }

        return contents;
    }

    private boolean isCurrentUser(User user) {
        return user != null && user.equals(getCurrentUser());
    }

    private Label getFavouriteLabel(User user) {
        return getLabelManager().getLabel(
                new ParsedLabelName(LabelManager.FAVOURITE_LABEL, user.getName(),
                        LabelParser.PERSONAL_LABEL_PREFIX));
    }

    @SupplierKey({"favorite spaces", "favourite spaces"})
    @API("1.0.0")
    public List<Space> getFavouriteSpaces(@KeyValue User user) {
        if (isCurrentUser(user) || isGlobalAdministrator(getCurrentUser())) {
            return getLabelManager().getFavouriteSpaces(user.getName());
        }
        return null;
    }

    @SupplierKey("authored pages")
    @API("1.0.0")
    public List<ContentEntityObject> getUserAuthoredPages(@KeyValue User user) {
        @SuppressWarnings("unchecked")
        List<ContentEntityObject> result = contentEntityObjectDao.getContentAuthoredByUser(user.getName());

        if (result == null || result.isEmpty())
            //noinspection unchecked
            return Collections.EMPTY_LIST;

        return getPermissionManager().getPermittedEntities(AuthenticatedUserThreadLocal.getUser(),
                Permission.VIEW, result);
    }

    @SupplierKey("personal information")
    @API("1.0.0")
    public PersonalInformation getUserPersonalInformation(@KeyValue User user) {
        return personalInformationManager.getPersonalInformation(user);
    }

    @SupplierKey("is removable")
    @API("1.0.0")
    public boolean isUserRemovable(@KeyValue User user) throws SupplierException {
        try {
            return getUserAccessor().isUserRemovable(user);
        } catch (EntityException e) {
            throw new SupplierException("Error while testing if user is removable.", e);
        }
    }

    @SupplierKey("groups")
    @API("1.0.0")
    public List<Group> getUserGroups(@KeyValue User user) {
        if (canViewPrivateInfo(user)) {

            Pager<Group> groups = getUserAccessor().getGroups(user);
            List<Group> groupList = new java.util.ArrayList<Group>();

            for (Group group : groups) {
                groupList.add(group);
            }

            return groupList;
        }
        return null;
    }

    protected boolean isGlobalAdministrator(User user) {
        return getPermissionManager().hasPermission(user,
                Permission.ADMINISTER,
                PermissionManager.TARGET_APPLICATION);
    }

    private boolean canViewPrivateInfo(User user) {
        User currentUser = getCurrentUser();
        return currentUser != null && (currentUser.equals(user) || isGlobalAdministrator(currentUser));
    }

    @SuppressWarnings("unchecked")
    @SupplierKey("labels")
    @API("1.0.0")
    public List<Label> getUserLabels(@KeyValue User user) {
        if (getCurrentUser().equals(user))
            return getLabelManager().getUsersLabels(user.getName());
        else
            return null;
    }

    @SupplierKey("email")
    @API("1.0.0")
    public Object getUserEmail(@KeyValue User user) {
        String emailAddress = user.getEmail();

        Settings globalSettings = settingsManager.getGlobalSettings();

        if (emailAddress == null || isGlobalAdministrator(getCurrentUser()) || Settings.EMAIL_ADDRESS_PUBLIC.equals(globalSettings.getEmailAddressVisibility())) {
            try {
                return new ConfluenceMailAddress(new InternetAddress(emailAddress, user.getFullName()));
            } catch (UnsupportedEncodingException e) {
                return emailAddress;
            }
        }

        if (Settings.EMAIL_ADDRESS_PRIVATE.equals(globalSettings.getEmailAddressVisibility()))
            return null;

        StringBuilder buf = new StringBuilder(emailAddress.length() + 20);

        for (int i = 0; i < emailAddress.length(); i++) {
            char c = emailAddress.charAt(i);
            if (c == '.')
                buf.append(" dot ");
            else if (c == '@')
                buf.append(" at ");
            else
                buf.append(c);
        }

        return buf.toString();
    }

    @SupplierKey("phone")
    @API("1.0.0")
    public String getPhone(@KeyValue User user) {
        return userDetailsManager.getStringProperty(user, "phone");
    }

    @SupplierKey("im")
    @API("1.0.0")
    public String getIM(@KeyValue User user) {
        return userDetailsManager.getStringProperty(user, "im");
    }
    @SupplierKey("website")
    @API("1.0.0")
    public String getWebsite(@KeyValue User user) {
        return userDetailsManager.getStringProperty(user, "website");
    }

    @SupplierKey("position")
    @API("1.0.0")
    public String getPosition(@KeyValue User user) {
        return userDetailsManager.getStringProperty(user, "position");
    }

    @SupplierKey("department")
    @API("1.0.0")
    public String getDepartment(@KeyValue User user) {
        return userDetailsManager.getStringProperty(user, "department");
    }

    @SupplierKey("location")
    @API("1.0.0")
    public String getLocation(@KeyValue User user) {
        return userDetailsManager.getStringProperty(user, "location");
    }

    @SupplierKey({"picture", "picture url"})
    @API("1.0.0")
    public String getPictureURL(@KeyValue User user) {
        if (user != null) {
            ProfilePictureInfo info = getUserAccessor().getUserProfilePicture(user);
            if (info != null)
                return info.getDownloadPath();
        }

        return null;
    }

    /**
     * Finds the URL which the specified value can link to for more information.
     * The URL may be relative to the Confluence install, or absolute. If none
     * is available, return <code>null</code>.
     *
     * @param context The context object.
     * @return The URL.
     * @throws SupplierException if there is a problem finding the value.
     */
    @Override
    public String getLink(SupplierContext context) throws SupplierException {
        User user = context.getValueAs(User.class);
        return user == null ? null : getUserURL(user);
    }

    @SupplierKey("url")
    private String getUserURL(@KeyValue User user) {
        return "/display/~" + user.getName();
    }

    public PageManager getPageManager() {
        return pageManager;
    }

    protected SpaceManager getSpaceManager() {
        return spaceManager;
    }

    @Override
    public String getDisplayText(SupplierContext context) throws SupplierException {
        User user = context.getValueAs(User.class);
        if (user == null)
            return null;

        if (isBlank(user.getFullName()))
            return user.getName();
        return user.getFullName();
    }

    private boolean isBlank(String value) {
        return value == null || value.trim().length() == 0;
    }
}
