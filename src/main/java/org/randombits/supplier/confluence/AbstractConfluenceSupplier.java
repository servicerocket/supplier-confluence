/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.confluence;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;
import org.randombits.supplier.core.annotate.AnnotatedSupplier;
import org.randombits.utils.lang.API;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Abstract base class for suppliers which work with Confluence content.
 *
 * @author David Peterson
 */
@API("1.0.0")
public abstract class AbstractConfluenceSupplier extends AnnotatedSupplier {

    private UserAccessor userAccessor;

    private BootstrapManager bootstrapManager;

    private PermissionManager permissionManager;

    private LabelManager labelManager;

    /**
     * Constructs a supplier that will use the {@link org.randombits.supplier.core.annotate.SupplierPrefix} and
     * {@link org.randombits.supplier.core.annotate.SupportedTypes} annotations to initialise itself.
     */
    @API("1.0.0")
    protected AbstractConfluenceSupplier() {
    }

    /**
     * Constructs an annotated supplier with the prefix and supported types values hard-wired.
     * Any {@link org.randombits.supplier.core.annotate.SupplierPrefix} or
     * {@link org.randombits.supplier.core.annotate.SupportedTypes} annotations will be ignored.
     *
     * @param prefix         The prefix to use.
     * @param prefixRequired <code>true</code> if the prefix must be used.
     * @param supportedTypes The list of types supported by this Supplier. If all types (even null) are supported, do not provide any values.
     */
    @API("1.0.0")
    public AbstractConfluenceSupplier( String prefix, boolean prefixRequired, Class<?>[] supportedTypes, boolean nullAllowed ) {
        super( prefix, prefixRequired, supportedTypes, nullAllowed );
    }

    @API("1.0.0")
    public AbstractConfluenceSupplier( Set<String> prefixes, boolean prefixRequired, Class<?>[] supportedTypes, boolean nullAllowed ) {
        super( prefixes, prefixRequired, supportedTypes, nullAllowed );
    }

    @Autowired
    public void setUserAccessor( UserAccessor userAccessor ) {
        this.userAccessor = userAccessor;
    }

    @API("1.0.0")
    protected UserAccessor getUserAccessor() {
        return userAccessor;
    }

    @Autowired
    public void setBootstrapManager( BootstrapManager bootstrapManager ) {
        this.bootstrapManager = bootstrapManager;
    }

    @Autowired
    protected BootstrapManager getBootstrapManager() {
        return bootstrapManager;
    }

    @Autowired
    public void setPermissionManager( PermissionManager permissionManager ) {
        this.permissionManager = permissionManager;
    }

    @API("1.0.0")
    protected PermissionManager getPermissionManager() {
        return permissionManager;
    }

    @Autowired
    public void setLabelManager( LabelManager labelManager ) {
        this.labelManager = labelManager;
    }

    @API("1.0.0")
    protected LabelManager getLabelManager() {
        return labelManager;
    }

    protected <T> List<T> getPermittedObjects( List<T> contentList ) {
        if ( contentList != null ) {
            Iterator<T> i = contentList.iterator();
            T content;

            while ( i.hasNext() ) {
                content = i.next();
                if ( !getPermissionManager().hasPermission( getCurrentUser(), Permission.VIEW, content ) )
                    i.remove();
            }
        }
        return contentList;
    }

    protected static User getCurrentUser() {
        return AuthenticatedUserThreadLocal.getUser();
    }

    protected List<Label> getPermittedLabels( List<Label> labels ) {
        if ( labels != null ) {
            Iterator<Label> i = labels.iterator();
            Label rel;
            User user = getCurrentUser();
            String username = user != null ? user.getName() : null;

            while ( i.hasNext() ) {
                rel = i.next();
                if ( !rel.isVisibleTo( username ) )
                    i.remove();
            }
        }

        return labels;
    }
}
