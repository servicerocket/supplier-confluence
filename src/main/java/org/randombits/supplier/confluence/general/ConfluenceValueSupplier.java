package org.randombits.supplier.confluence.general;

import java.util.ArrayList;
import java.util.List;

import org.randombits.supplier.confluence.AbstractConfluenceSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.annotate.KeyContext;
import org.randombits.supplier.core.annotate.KeyParam;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.utils.lang.API;
import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.renderer.PageContext;

/**
 * Provides Confluence-specific 'value' creation.
 */
@SupplierPrefix(value = "value", required = true)
@API("1.0.0")
public class ConfluenceValueSupplier extends AbstractConfluenceSupplier {

    private LinkAssistant linkAssistant;
    
    @Autowired
    public void setLinkAssistant(LinkAssistant linkAssistant){ 
    	this.linkAssistant = linkAssistant; 
    }
    
    @SupplierKey("content {id}")
    @API("1.0.0")
    public ConfluenceEntityObject getContent(@KeyContext SupplierContext context, @KeyParam("id") String id) {
        ContentEntityObject currentContent = ( ContentEntityObject ) context.getValue();
        PageContext pageContext = currentContent != null ? currentContent.toPageContext() : new PageContext();
        ConversionContext conversionContext = new DefaultConversionContext(pageContext);
        return linkAssistant.getEntityForWikiLink( conversionContext, id );
    }
    
    @SupplierKey("label {value}")
    @API("1.0.0")
    public Label getLabel(@KeyContext SupplierContext context, @KeyParam("value") String value) {
        Label label = getLabelManager().getLabel(value);
        List<Label> labels = new ArrayList<Label>();
        labels.add(label);
        labels = getPermittedLabels(labels);
        
        if ( !labels.isEmpty() )
        	return labels.get(0);
        
        return null;
    }
}
