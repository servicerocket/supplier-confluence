/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.supplier.confluence.general;

import com.atlassian.confluence.mail.address.ConfluenceMailAddress;
import org.randombits.supplier.core.DisplayableSupplier;
import org.randombits.supplier.core.LinkableSupplier;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.SupplierException;
import org.randombits.supplier.core.annotate.*;
import org.randombits.utils.lang.API;

/**
 * Supplies information about {@link ConfluenceMailAddress} values. These match those available via the
 * {@link org.randombits.supplier.core.general.InternetAddressSupplier} and use the same 'email' prefix.
 *
 * @author David Peterson
 */
@SupplierPrefix("email")
@SupportedTypes(ConfluenceMailAddress.class)
@API("1.0.0")
public class ConfluenceMailAddressSupplier extends AnnotatedSupplier implements LinkableSupplier, DisplayableSupplier {

    private static final String MAILTO_PREFIX = "mailto:";

    @SupplierKey("personal")
    @API("1.0.0")
    public String getPersonal( @KeyValue ConfluenceMailAddress address ) {
        return address.getPersonal();
    }

    @SupplierKey("address")
    @API("1.0.0")
    public String getAddress( @KeyValue ConfluenceMailAddress address ) {
        return address.getAddress();
    }

    @SupplierKey("url")
    @API("1.0.0")
    public String getUrl( @KeyValue ConfluenceMailAddress address ) {
        return address != null ? MAILTO_PREFIX + getAddress( address ) : null;
    }

    /**
     * Finds the URL which the specified value can link to for more information.
     * The URL may a server-relative link, or absolute. If none is available,
     * return <code>null</code>.
     *
     * @param context The context object.
     * @return The URL.
     * @throws SupplierException if there is a problem finding the value.
     */
    @Override
    public String getLink( SupplierContext context ) throws SupplierException {
        ConfluenceMailAddress address = context.getValueAs( ConfluenceMailAddress.class );
        return address == null ? null : getUrl( address );
    }

    @Override
    public String getDisplayText( SupplierContext context ) throws SupplierException {
        ConfluenceMailAddress address = context.getValueAs( ConfluenceMailAddress.class );
        return address == null ? null : getAddress( address );
    }
}
