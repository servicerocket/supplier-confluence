package org.randombits.supplier.confluence.general;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.User;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.confluence.AbstractConfluenceSupplier;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.utils.lang.API;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

/**
 * Provides access to global state from within Confluence.
 */
@SupplierPrefix({"global", "confluence"})
@API("1.0.0")
public class ConfluenceGlobalSupplier extends AbstractConfluenceSupplier {

    private EnvironmentAssistant environmentAssistant;
 
    private SpaceManager spaceManager;

    public ConfluenceGlobalSupplier(EnvironmentAssistant environmentAssistant, SpaceManager spaceManager) {
        this.environmentAssistant = environmentAssistant;
        this.spaceManager = spaceManager;
    }
    
    @Autowired
    public void setSpaceManager(SpaceManager spaceManager){ 
    	this.spaceManager = spaceManager; 
    }
    
    @Autowired
    public void setEnvironmentAssistant(EnvironmentAssistant environmentAssistant){ 
    	this.environmentAssistant = environmentAssistant; 
    }
    
    @SupplierKey("current user")
    @API("1.0.0")
    public User getCurrentAtlassianUser() {
        return environmentAssistant.getValue(User.class);
    }

    @SupplierKey("all spaces")
    @API("1.0.0")
    public Collection<Space> getAllSpaces() {
        return getPermittedObjects(spaceManager.getAllSpaces());
    }
}
