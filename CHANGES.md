Supplier Confluence
===================

2016-01-13, 1.4.0
-----------------
 * WIKI-640 New `categories` Space Supplier to report space category.

2015-12-01, 1.3.0
-----------------
 * WIKI-584 Compatible with Confluence 5.9.1.

2014-12-16, 1.2.0
-----------------
 * WIKI-536 Change User Supplier getUserEmail method to return from InternetAddress to ConfluenceMailAddress object.
