[ ![Codeship Status for servicerocket/supplier-confluence](https://www.codeship.io/projects/2fad6610-3cb4-0132-98d3-6e1a86199194/status)](https://www.codeship.io/projects/43036)

This is a plugin for Atlassian Confluence that provides Confluence-specific Suppliers. Suppliers provide
access to data and other values within an application in a simple but flexible manner. For more details
visit the main "Supplier - Core" library at:

https://bitbucket.org/randomeizer/supplier-core

For more details about this plugin, visit the wiki:

https://bitbucket.org/randomeizer/supplier-confluence/wiki
