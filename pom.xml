<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.servicerocket.randombits</groupId>
        <artifactId>randombits</artifactId>
        <version>7</version>
        <relativePath />
    </parent>

    <groupId>org.randombits.supplier</groupId>
    <artifactId>supplier-confluence</artifactId>
    <version>1.4.0</version>

    <name>RB Supplier - Confluence</name>
    <description>This plugin provides Confluence-specific Suppliers.</description>
    <packaging>atlassian-plugin</packaging>
    <url>https://bitbucket.org/servicerocket/supplier-confluence</url>

    <organization>
        <name>randombits.org</name>
        <url>http://www.randombits.org/</url>
    </organization>

    <developers>
        <developer>
            <name>David Peterson</name>
            <organization>randombits.org</organization>
            <roles>
                <role>Developer</role>
            </roles>
        </developer>
    </developers>

    <licenses>
        <license>
            <name>New BSD License</name>
            <url>http://www.opensource.org/licenses/bsd-license.php</url>
        </license>
    </licenses>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/servicerocket/supplier-confluence.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/servicerocket/supplier-confluence.git</developerConnection>
        <url>https://bitbucket.org/servicerocket/supplier-confluence</url>
        <tag>1.4.0</tag>
    </scm>

    <properties>
        <platform.minVersion>5.4</platform.minVersion>
        <platform.maxVersion>5.9.1</platform.maxVersion>
        <project.build.sourceRootDirectory>${project.basedir}/src/main</project.build.sourceRootDirectory>
        <randombits.test.version>1</randombits.test.version>

        <storage.version>5.5.0</storage.version>
        <storage.version.range>[5.5.0,6)</storage.version.range>
        <support.core.version>1.4.0</support.core.version>
        <support.core.version.range>[1.4.0,2)</support.core.version.range>
        <support.confluence.version>5.4.0</support.confluence.version>
        <support.confluence.version.range>[5.4.0,6)</support.confluence.version.range>
        <supplier.core.version>1.3.0</supplier.core.version>
        <supplier.core.version.range>[1.3.0,2)</supplier.core.version.range>

        <javax.servlet.version>3.1.0</javax.servlet.version>
        <javax.servlet.version.range>[2.4,4)</javax.servlet.version.range>
    </properties>

    <dependencies>
        <!-- Bundled Dependencies -->
        <dependency>
            <groupId>org.randombits.utils</groupId>
            <artifactId>rb-utils</artifactId>
            <version>2.0.0</version>
        </dependency>

        <!-- OSGi Dependencies -->
        <dependency>
            <groupId>org.randombits.supplier</groupId>
            <artifactId>supplier-core</artifactId>
            <version>${supplier.core.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.randombits.support</groupId>
            <artifactId>support-core</artifactId>
            <version>${support.core.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.randombits.support</groupId>
            <artifactId>support-confluence</artifactId>
            <version>${support.confluence.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.randombits.storage</groupId>
            <artifactId>storage</artifactId>
            <version>${storage.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.randombits.storage</groupId>
            <artifactId>storage-servlet</artifactId>
            <version>${storage.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.randombits.storage</groupId>
            <artifactId>storage-confluence</artifactId>
            <version>${storage.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>2.5.2</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>${javax.servlet.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>1.6.1</version>
        </dependency>

        <!-- Testing -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence</artifactId>
            <version>${confluence.version}</version>
        </dependency>

        <dependency>
            <groupId>com.servicerocket.randombits</groupId>
            <artifactId>randombits-test</artifactId>
            <version>${randombits.test.version}</version>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>atlassian-confluence-pageobjects</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.upm</groupId>
            <artifactId>atlassian-universal-plugin-manager-pageobjects</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.selenium</groupId>
            <artifactId>atlassian-webdriver-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-webdriver-support</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <version>4.3.5</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>18.0</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>${project.build.sourceRootDirectory}/resources</directory>
                <excludes>
                    <exclude>atlassian-plugin.xml</exclude>
                </excludes>
            </resource>
            <resource>
                <directory>${project.build.sourceRootDirectory}/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>atlassian-plugin.xml</include>
                </includes>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-confluence-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <pluginDependencies>
                        <pluginDependency>
                            <groupId>org.randombits.supplier</groupId>
                            <artifactId>supplier-core</artifactId>
                        </pluginDependency>
                        <pluginDependency>
                            <groupId>org.randombits.support</groupId>
                            <artifactId>support-confluence</artifactId>
                        </pluginDependency>
                        <pluginDependency>
                            <groupId>org.randombits.support</groupId>
                            <artifactId>support-core</artifactId>
                        </pluginDependency>
                        <pluginDependency>
                            <groupId>org.randombits.storage</groupId>
                            <artifactId>storage</artifactId>
                        </pluginDependency>
                        <pluginDependency>
                            <groupId>org.randombits.storage</groupId>
                            <artifactId>storage-servlet</artifactId>
                        </pluginDependency>
                        <pluginDependency>
                            <groupId>org.randombits.storage</groupId>
                            <artifactId>storage-confluence</artifactId>
                        </pluginDependency>
                    </pluginDependencies>

                    <instructions>
                        <Export-Package>org.randombits.supplier.confluence.*;version="${project.version}"</Export-Package>
                        <Import-Package>
                            org.randombits.supplier.confluence.*;version="${project.version}",
                            org.randombits.supplier.core.*;version="${supplier.core.version.range}",
                            org.randombits.storage.confluence;version="${storage.version.range}",
                            org.randombits.storage.servlet;version="${storage.version.range}",
                            org.randombits.storage;version="${storage.version.range}",
                            org.randombits.support.core.*;version="${support.core.version.range}",
                            org.randombits.support.confluence.*;version="${support.confluence.version.range}",
                            com.atlassian.confluence.*;version="[4,7)",
                            com.atlassian.spring.container.*;version="2.0.0",
                            com.atlassian.user.*;version="3.0",
                            org.apache.commons.collections.*;version="3.2",
                            org.apache.commons.lang.*;version="2.4",
                            org.apache.log4j.*;version="1.2",
                            org.springframework.beans.*;version="2.5",
                            com.opensymphony.module.propertyset.*;version="1.3",
                            javax.servlet.*;version="${javax.servlet.version.range}",
                            javax.mail.internet.*;version=1.4.1
                        </Import-Package>
                        <Spring-Context>*;timeout:=60</Spring-Context>
                        <CONF_COMM />
                    </instructions>
                    <testGroups>
                        <testGroup>
                            <id>supplier-confluence-it</id>
                            <productIds>
                                <productId>confluence</productId>
                            </productIds>
                            <includes>
                                <include>it/**/*Test.java</include>
                            </includes>
                        </testGroup>
                    </testGroups>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <!-- Required for deployment to Sonatype -->
            <id>release-sign-artifacts</id>
            <activation>
                <property>
                    <name>performRelease</name>
                    <value>true</value>
                </property>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

</project>
